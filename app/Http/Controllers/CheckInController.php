<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use App\CheckIns;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Pagination\LengthAwarePaginator;

class CheckInController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        $checkins = \DB::table('check_ins as ch')
            ->join('branches as br', 'br.branch_id', '=', 'ch.branch_id')
            ->join('services as sv', 'sv.service_id', '=', 'ch.service_id')
            ->select(
                    'check_in_id',
                    'ch.name',
                    'ch.email',
                    'ch.phone_no',
                    'ch.no_of_guest',
                    'sv.name as sv_name',
                    'sv.price',
                    'br.address',
                    'br.landmark',
                    'br.telephone_number'
                )
            ->paginate(5);
        return view('checkins.index',[
            'checkins' => $checkins
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  Request  $request
     * @return Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  Request  $request
     * @param  int  $id
     * @return Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id)
    {
        //
    }
}
