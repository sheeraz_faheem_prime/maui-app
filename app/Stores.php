<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Stores extends Model
{


 	/**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'stores';
    

    /**
     * This a primary key
     *
     * @var string
     */
    protected $primaryKey = 'store_id';

   
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [];

    
    /**
     * Get the _stores record associated with the _branches.
     *
     * @return Response
     */
    public function branches()
    {
    	return $this->hasMany('App\Branches', 'store_id');
    }

}