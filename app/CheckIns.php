<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CheckIns extends Model
{
    
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'check_ins';
    

    /**
     * This a primary key
     *
     * @var string
     */
    protected $primaryKey = 'check_in_id';

   
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['branch_id', 'service_id', 'name', 'email', 'phone_no'];
}