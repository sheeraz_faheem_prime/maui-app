-- phpMyAdmin SQL Dump
-- version 4.1.3
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Sep 15, 2015 at 04:21 PM
-- Server version: 5.5.44-0+deb7u1
-- PHP Version: 5.6.13-1~dotdeb+zts+7.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `nikki_mauisalon`
--

-- --------------------------------------------------------

--
-- Table structure for table `mss_branches`
--

DROP TABLE IF EXISTS `mss_branches`;
CREATE TABLE IF NOT EXISTS `mss_branches` (
  `branch_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `store_id` int(11) NOT NULL,
  `address` text COLLATE utf8_unicode_ci NOT NULL,
  `landmark` text COLLATE utf8_unicode_ci NOT NULL,
  `store_hours` text COLLATE utf8_unicode_ci NOT NULL,
  `telephone_number` varchar(32) COLLATE utf8_unicode_ci NOT NULL,
  `status` varchar(16) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'active',
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`branch_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=11 ;

--
-- Dumping data for table `mss_branches`
--

INSERT INTO `mss_branches` (`branch_id`, `store_id`, `address`, `landmark`, `store_hours`, `telephone_number`, `status`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 1, '199 E. Alex Bell Rd. Unit 402, Centerville Ohio 45459', 'next to Doubleday''s and Firestone', '', '(937) 401-0710', '1', '2015-08-06 04:00:00', '2015-08-06 04:00:00', NULL),
(2, 2, '7967 Cincinnati Dayton Rd Unit J, West Chester Ohio 45069', '', '', '(513) 847-6008', '1', '2015-08-06 04:00:00', '2015-08-06 04:00:00', NULL),
(3, 3, '3092 Madison Rd., Cincinnati Ohio 45209', '', '', '(513) 788-2212', '1', '2015-08-06 04:00:00', '2015-08-06 04:00:00', NULL),
(4, 4, '9247 E 141 st. Fishers Indiana 46038', '', '', '(317) 214-7829', '1', '2015-08-06 04:00:00', '2015-08-06 04:00:00', NULL),
(5, 5, '8660 Guion Rd, Indianapolis Indiana 46268', '', '', '(317) 451-4212', '1', '2015-08-06 04:00:00', '2015-08-06 04:00:00', NULL),
(6, 6, '4113 Oechsli Ave Suite F, Louisville Kentucky 40207', '', '', '(502) 938-MAUI or (502) 938-6284', '1', '2015-08-06 04:00:00', '2015-08-06 04:00:00', NULL),
(7, 7, '330 Louisiana Ave. Suite E, Perrysburg Ohio 43551', '', '', '(419) 265-0699', '1', '2015-08-06 04:00:00', '2015-08-06 04:00:00', NULL),
(8, 8, '97 N. Kingshighway Suite 6, Cape Girardeau Missouri 63701', '', '', '573-803-1818', '1', '2015-08-06 04:00:00', '2015-08-06 04:00:00', NULL),
(9, 9, 'Eastern Hills Mall 4545 Transit Rd, Williamsville New York 14221', '', '', '(716) 580-7066', '1', '2015-08-06 04:00:00', '2015-08-06 04:00:00', NULL),
(10, 10, '12751 Marblestone Drive, Suite 240, Woodbridge Virginia 22192', '', '', '(703) 730-6284', '1', '2015-08-06 04:00:00', '2015-08-06 04:00:00', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `mss_check_ins`
--

DROP TABLE IF EXISTS `mss_check_ins`;
CREATE TABLE IF NOT EXISTS `mss_check_ins` (
  `check_in_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `branch_id` int(10) unsigned NOT NULL,
  `service_id` int(10) unsigned NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `phone_no` varchar(32) COLLATE utf8_unicode_ci NOT NULL,
  `no_of_guest` smallint(5) unsigned NOT NULL,
  `status` varchar(16) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'active',
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`check_in_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=22 ;

--
-- Dumping data for table `mss_check_ins`
--

INSERT INTO `mss_check_ins` (`check_in_id`, `branch_id`, `service_id`, `name`, `email`, `phone_no`, `no_of_guest`, `status`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 1, 1, 'Jemuel Test', 'jemuel@molavenet.com', '4526698', 0, 'active', '2015-09-01 10:19:44', '2015-09-01 10:19:44', NULL),
(2, 1, 1, 'Jemuel Test', 'jemuel@molavenet.com', '4526698', 0, 'active', '2015-09-01 10:21:40', '2015-09-01 10:21:40', NULL),
(3, 7, 1, 'Marc Jesus Caabay', 'mj@molavenet.com', '09264800000', 0, 'active', '2015-09-01 19:54:11', '2015-09-01 19:54:11', NULL),
(4, 7, 1, 'Marc Jesus Caabay', 'mj@molavenet.com', '09264800000', 0, 'active', '2015-09-01 19:54:56', '2015-09-01 19:54:56', NULL),
(5, 1, 1, 'Jemuel Rodriguez', 'jemuel@molavenet.com', '4526698', 0, 'active', '2015-09-01 19:59:50', '2015-09-01 19:59:50', NULL),
(6, 7, 1, 'Marc Jesus Caabay', 'mj@molavenet.com', '09264800000', 0, 'active', '2015-09-01 20:01:27', '2015-09-01 20:01:27', NULL),
(7, 7, 1, 'Marc Jesus', 'email@y.c', '092000', 0, 'active', '2015-09-01 20:03:36', '2015-09-01 20:03:36', NULL),
(8, 7, 1, 'Marc Jesus Caabay', 'mj@molavenet.com', '09264800000', 0, 'active', '2015-09-01 20:07:03', '2015-09-01 20:07:03', NULL),
(9, 1, 1, 'Richard Chan', 'richard@test.com', '4526698', 25, 'active', '2015-09-03 14:54:59', '2015-09-03 14:54:59', NULL),
(10, 7, 4, 'Marc J', 'mj@yahoo.com', '12345677', 3, 'active', '2015-09-03 15:04:16', '2015-09-03 15:04:16', NULL),
(11, 10, 1, 'mja', 'asd@yahoo.com', '12345', 2, 'active', '2015-09-03 15:18:18', '2015-09-03 15:18:18', NULL),
(12, 10, 1, 'mjaaaa', 'asd@yahoo.com', '12345', 2, 'active', '2015-09-03 15:19:23', '2015-09-03 15:19:23', NULL),
(13, 7, 1, 'mjm', 'aaa@yahoo.com', '123124', 0, 'active', '2015-09-03 15:39:54', '2015-09-03 15:39:54', NULL),
(14, 7, 4, 'Marc J', 'mj@yahoo.com', '123566', 3, 'active', '2015-09-03 15:43:15', '2015-09-03 15:43:15', NULL),
(15, 7, 4, 'mnjs', 'aa@y.c', '12354', 3, 'active', '2015-09-03 15:46:30', '2015-09-03 15:46:30', NULL),
(16, 10, 3, 'Denise', 'Den@gmail.com', '098761462', 5, 'active', '2015-09-03 16:55:32', '2015-09-03 16:55:32', NULL),
(17, 1, 2, 'Marc Jesus', 'mj@yahoo.com', '0912345678', 1, 'active', '2015-09-03 20:50:07', '2015-09-03 20:50:07', NULL),
(18, 10, 1, 'crrjgg', 'cajjh@y.c', 'b367', 2, 'active', '2015-09-04 19:39:09', '2015-09-04 19:39:09', NULL),
(19, 10, 3, 'mj caabay', 'mj@yahoo.com', '039744546', 4, 'active', '2015-09-04 20:06:30', '2015-09-04 20:06:30', NULL),
(20, 1, 1, 'jjj', 'jj@y.c', '0983556', 5, 'active', '2015-09-04 20:07:15', '2015-09-04 20:07:15', NULL),
(21, 6, 2, 'may', 'may@y.c', '0917585865', 1, 'active', '2015-09-12 00:33:43', '2015-09-12 00:33:43', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `mss_migrations`
--

DROP TABLE IF EXISTS `mss_migrations`;
CREATE TABLE IF NOT EXISTS `mss_migrations` (
  `migration` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `mss_migrations`
--

INSERT INTO `mss_migrations` (`migration`, `batch`) VALUES
('2014_10_12_000000_create_users_table', 1),
('2014_10_12_100000_create_password_resets_table', 1),
('2015_08_09_232147_create_posts_table', 1),
('2015_08_14_192536_create_branches_table', 1),
('2015_08_15_230000_create_check_in_table', 1),
('2015_08_15_232437_create_services_table', 1),
('2015_08_15_232843_create_services_offer_table', 1),
('2015_08_15_233145_create_stores_table', 1),
('2014_10_12_000000_create_users_table', 1),
('2014_10_12_100000_create_password_resets_table', 1),
('2015_08_09_232147_create_posts_table', 1),
('2015_08_14_192536_create_branches_table', 1),
('2015_08_15_230000_create_check_in_table', 1),
('2015_08_15_232437_create_services_table', 1),
('2015_08_15_232843_create_services_offer_table', 1),
('2015_08_15_233145_create_stores_table', 1);

-- --------------------------------------------------------

--
-- Table structure for table `mss_password_resets`
--

DROP TABLE IF EXISTS `mss_password_resets`;
CREATE TABLE IF NOT EXISTS `mss_password_resets` (
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  KEY `password_resets_email_index` (`email`),
  KEY `password_resets_token_index` (`token`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `mss_posts`
--

DROP TABLE IF EXISTS `mss_posts`;
CREATE TABLE IF NOT EXISTS `mss_posts` (
  `post_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(10) unsigned NOT NULL,
  `content` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `status` varchar(16) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'active',
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`post_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=5 ;

--
-- Dumping data for table `mss_posts`
--

INSERT INTO `mss_posts` (`post_id`, `user_id`, `content`, `status`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 1, 'tex\r\n', 'active', '2015-08-19 12:36:58', '2015-08-19 12:36:58', NULL),
(2, 1, 'data\r\n', 'active', '2015-08-20 09:44:51', '2015-08-20 09:44:51', NULL),
(3, 1, 'help', 'active', '2015-08-20 09:45:57', '2015-08-20 09:45:57', NULL),
(4, 1, 'test', 'active', '2015-08-20 09:46:41', '2015-08-20 09:46:41', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `mss_services`
--

DROP TABLE IF EXISTS `mss_services`;
CREATE TABLE IF NOT EXISTS `mss_services` (
  `service_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `price` double(8,2) NOT NULL,
  `status` varchar(16) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'active',
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`service_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=5 ;

--
-- Dumping data for table `mss_services`
--

INSERT INTO `mss_services` (`service_id`, `name`, `price`, `status`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'Laser Teeth Whitening', 0.00, '1', '2015-08-07 04:00:00', '2015-08-07 04:00:00', NULL),
(2, 'Eyelash Extensions', 0.00, '1', '2015-08-07 04:00:00', '2015-08-07 04:00:00', NULL),
(3, 'Medical Weight Loss', 0.00, '1', '2015-08-07 04:00:00', '2015-08-07 04:00:00', NULL),
(4, 'Botox/Fillers', 0.00, '1', '2015-08-07 04:00:00', '2015-08-07 04:00:00', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `mss_services_offer`
--

DROP TABLE IF EXISTS `mss_services_offer`;
CREATE TABLE IF NOT EXISTS `mss_services_offer` (
  `service_offer_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `service_id` int(10) unsigned NOT NULL,
  `branch_id` int(10) unsigned NOT NULL,
  `status` varchar(16) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'active',
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`service_offer_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=21 ;

--
-- Dumping data for table `mss_services_offer`
--

INSERT INTO `mss_services_offer` (`service_offer_id`, `service_id`, `branch_id`, `status`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 1, 1, 'active', '2015-08-15 04:00:00', '2015-08-15 04:00:00', NULL),
(2, 2, 1, 'active', '2015-08-15 04:00:00', '2015-08-15 04:00:00', NULL),
(3, 3, 2, 'active', '2015-08-15 04:00:00', '2015-08-15 04:00:00', NULL),
(4, 4, 2, 'active', '2015-08-15 04:00:00', '2015-08-15 04:00:00', NULL),
(5, 1, 3, 'active', '2015-08-15 04:00:00', '2015-08-15 04:00:00', NULL),
(6, 3, 3, 'active', '2015-08-15 04:00:00', '2015-08-15 04:00:00', NULL),
(7, 2, 4, 'active', '2015-08-15 04:00:00', '2015-08-15 04:00:00', NULL),
(8, 4, 4, 'active', '2015-08-15 04:00:00', '2015-08-15 04:00:00', NULL),
(9, 1, 5, 'active', '2015-08-15 04:00:00', '2015-08-15 04:00:00', NULL),
(10, 1, 6, 'active', '2015-08-15 04:00:00', '2015-08-15 04:00:00', NULL),
(11, 2, 6, 'active', '2015-08-15 04:00:00', '2015-08-15 04:00:00', NULL),
(12, 4, 6, 'active', '2015-08-15 04:00:00', '2015-08-15 04:00:00', NULL),
(13, 1, 7, 'active', '2015-08-15 04:00:00', '2015-08-15 04:00:00', NULL),
(14, 4, 7, 'active', '2015-08-15 04:00:00', '2015-08-15 04:00:00', NULL),
(15, 3, 8, 'active', '2015-08-15 04:00:00', '2015-08-15 04:00:00', NULL),
(16, 4, 8, 'active', '2015-08-15 04:00:00', '2015-08-15 04:00:00', NULL),
(17, 2, 9, 'active', '2015-08-15 04:00:00', '2015-08-15 04:00:00', NULL),
(18, 1, 10, 'active', '2015-08-15 04:00:00', '2015-08-15 04:00:00', NULL),
(19, 3, 10, 'active', '2015-08-15 04:00:00', '2015-08-15 04:00:00', NULL),
(20, 4, 10, 'active', '2015-08-15 04:00:00', '2015-08-15 04:00:00', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `mss_stores`
--

DROP TABLE IF EXISTS `mss_stores`;
CREATE TABLE IF NOT EXISTS `mss_stores` (
  `store_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `user_id` int(10) unsigned NOT NULL,
  `status` varchar(16) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'active',
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`store_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=11 ;

--
-- Dumping data for table `mss_stores`
--

INSERT INTO `mss_stores` (`store_id`, `name`, `user_id`, `status`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'Maui Whitening Dayton', 3, 'active', '2015-08-15 04:00:00', '2015-08-15 04:00:00', NULL),
(2, 'Maui Whitening West Chester', 4, 'active', '2015-08-15 04:00:00', '2015-08-15 04:00:00', NULL),
(3, 'Maui Whitening Hyde Park / Oakley', 4, 'active', '2015-08-15 04:00:00', '2015-08-15 04:00:00', NULL),
(4, 'Maui Whitening Fishers', 3, 'active', '2015-08-15 04:00:00', '2015-08-15 04:00:00', NULL),
(5, 'Maui Whitening at Indy Health & Fitness', 4, 'active', '2015-08-15 04:00:00', '2015-08-15 04:00:00', NULL),
(6, 'Maui Whitening Louisville', 3, 'active', '2015-08-15 04:00:00', '2015-08-15 04:00:00', NULL),
(7, 'Maui Whitening Perrysburg', 3, 'active', '2015-08-15 04:00:00', '2015-08-15 04:00:00', NULL),
(8, 'Maui Whitening Cape Girardeau', 3, 'active', '2015-08-15 04:00:00', '2015-08-15 04:00:00', NULL),
(9, 'Maui Whitening Williamsville', 4, 'active', '2015-08-15 04:00:00', '2015-08-15 04:00:00', NULL),
(10, 'Maui Whitening Prince William', 4, 'active', '2015-08-15 04:00:00', '2015-08-15 04:00:00', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `mss_users`
--

DROP TABLE IF EXISTS `mss_users`;
CREATE TABLE IF NOT EXISTS `mss_users` (
  `user_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(60) COLLATE utf8_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`user_id`),
  UNIQUE KEY `users_email_unique` (`email`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=3 ;

--
-- Dumping data for table `mss_users`
--

INSERT INTO `mss_users` (`user_id`, `name`, `email`, `password`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'Jemuel Rodriguez', 'jemuel@primeoutsourcing.com', '$2y$10$Q3qjZGAg7TnONy1YzcvX5ObNHUxsrFoBUOYbj2o7evNg8K5DKpuIi', 'CE7PFsgwokUPWLR2XhuZk0xPLwjKfibiBpF3AmUpeeMLKWxVjrxw7PFRPP1W', '2015-08-19 09:42:44', '2015-08-19 09:49:40'),
(2, 'Dummy Accnt', 'dummyaccnt548@gmail.com', '$2y$10$CP18b7WkU6HAVv27/pc.TOidtf1vO71qsXxKjlizncGSV8eEOl9h2', NULL, '2015-08-24 05:09:17', '2015-08-24 05:09:17');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
