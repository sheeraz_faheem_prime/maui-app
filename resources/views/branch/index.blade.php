@extends('layout')

@section('content')

    @include('_partial.header')
    
    @include('_partial.sidebar_left')

    <!--main content start-->
    <section id="main-content">
        <section class="wrapper">
            <!-- page start-->

            @if (Session::has('flash_message'))
            <div class="alert alert-success alert-block fade in">
                <button type="button" class="close close-sm" data-dismiss="alert">
                    <i class="fa fa-times"></i>
                </button>
                <h4>
                    <i class="fa fa-ok-sign"></i>
                    Success!
                </h4>
                <p>{{ Session::get('flash_message') }}</p>
            </div>
            @endif

            <div class="row">
                  <div class="col-lg-12">
                      <section class="panel">
                          <header class="panel-heading">
                              Our Branches
                          </header>
                          <div class="panel-body">
                              <section id="unseen">
                                <table class="table table-bordered table-striped table-condensed">
                                  <thead>
                                  <tr>
                                      <th>ID</th>
                                      <th>Store Name</th>
                                      <th>Address</th>
                                      <th>Landmark</th>
                                      <th>Store Hours</th>
                                      <th>Telephone No.</th>
                                  </tr>
                                  </thead>
                                  <tbody>
                                 @foreach($data as $branch)
                                  <tr>
                                      <td>{{$branch->branch_id}}</td>
                                      <td>{{$branch->name}}</td>
                                      <td>{{$branch->address}}</td>
                                      <td>{{$branch->landmark}}</td>                                      
                                      <td>{{$branch->store_hours}}</td>
                                      <td>{{$branch->telephone_number}}</td>
                                  </tr>
                                  @endforeach
                                  </tbody>
                              </table>
                              <?php echo $data->render(); ?>
                              </section>
                          </div>
                      </section>
                  </div>
              </div>
        </section>
    </section>
    <!--main content end-->

    @include('_partial.slidebar_right')

    @include('_partial.footer')
@endsection
